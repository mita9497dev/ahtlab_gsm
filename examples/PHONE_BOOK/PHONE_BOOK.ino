#include <WiFi.h>

/* ======================================== */
/* SIM800 */
/* ======================================== */
#include <aht_sim800.h>
#define MODEM_RX 16
#define MODEM_TX 17
#define uart Serial2

AHT_GSM *gsmMaster = new AHT_GSM(&uart);
AHT_GSM *gsm;
GSM_TYPE gsmType;
bool gsmOk = false;

/* ======================================== */
/* PHONE BOOK */
/* ======================================== */
#define MAX_PHONE 15

PHONE_BOOK phoneBook[MAX_PHONE];
uint8_t numPhone = 0;

void setup() 
{
    Serial.begin(115200);
    delay(1000);

    if(gsmMaster->begin()) 
    {
        gsmType = gsmMaster->detectGSM(&uart);
        unsigned long baudrate = gsmMaster->getBaudrate();
        free(gsmMaster);
        
        if(gsmType == SIM800) 
        {
            gsm = new AHT_SIM800(&uart);
            gsm->begin(baudrate);

            gsm->println("AT&FZ");
            if (gsm->readResponse(1000, "OK") == AT_REPLY_FOUND)
            {
                gsmOk = true;
            }
        }

        delay(5000);
    }

    if (gsmOk)
    {
        WiFi_Init();
    }

    GSM_ReadPhoneBook();
}

void loop()
{
    if(!gsmOk)
    {
        Serial.println("can't detect gsm module");
        delay(5000);
        return;
    }

    
}

void WiFi_Init()
{
    WiFi.mode(WIFI_STA);
    WiFi.config(WiFi.localIP(), WiFi.gatewayIP(), WiFi.subnetMask(), IPAddress(8, 8, 8, 8));
}

void GSM_Reset()
{
    gsm->reset();
}

bool GSM_AddPhoneBook(PHONE_BOOK p)
{
    return gsm->addPhoneBook(p);
}

bool GSM_RemovePhoneBook(uint8_t index)
{
    for (uint8_t i = 0; i < numPhone; i++)
    {
        if (phoneBook[i].index == index)
        {
            return gsm->removePhoneBook(index);       
        }
    }

    return false;
}

void GSM_ReadPhoneBook()
{
    numPhone = gsm->readPhoneBook(phoneBook, 1, 10);

    for (uint8_t i = 0; i < numPhone; i++)
    {
        Serial.print("phoneBook[i].index -> ");
        Serial.println(phoneBook[i].index);
        Serial.print("phoneBook[i].phone -> ");
        Serial.println(phoneBook[i].phone);
    }
}